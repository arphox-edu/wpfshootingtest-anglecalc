﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ShootingTest
{
    class Bullet
    {
        public Ellipse Ellipse { get; set; }

        public double X { get; private set; }
        public double Y { get; private set; }

        public double StepX { get; private set; }
        public double StepY { get; private set; }

        public Bullet(int X, int Y, double StepX, double StepY)
        {
            Ellipse = new Ellipse();
            Ellipse.Fill = Brushes.Blue;
            Ellipse.Stroke = Brushes.Blue;
            Ellipse.Width = 8;
            Ellipse.Height = 8;

            this.X = X;
            this.Y = Y;
            this.StepX = StepX;
            this.StepY = StepY;
        }

        public void Move()
        {
            X += StepX;
            Y += StepY;

            Canvas.SetLeft(Ellipse, X);
            Canvas.SetTop(Ellipse, Y);
        }
    }
}