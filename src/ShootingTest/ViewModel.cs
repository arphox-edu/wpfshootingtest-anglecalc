﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootingTest
{
    class ViewModel : Bindable
    {
        public int MyX { get; set; } = 600;
        public int MyY { get; set; } = 300;
        public int TargetX { get; set; } = 800;
        public int TargetY { get; set; } = 500;
        private double _angle;
        public double Angle
        {
            get { return _angle; }
            set { _angle = value; OnPropertyChanged("Angle"); }
        }

        public ViewModel()
        {

        }

        public void CalculateAngle()
        {
            double a = MyY - TargetY;
            double b = MyX - TargetX;
            Angle = Math.Atan(a / b) * 180 / Math.PI;
            if (TargetX <= MyX)
                Angle += 180;
        }
    }
}